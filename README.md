# Kafka-basic-pub-sub



## Name
Kafka-basic-pub-sub

## Description
This is a simple kafka publish - subscription java spring boot, maven application. It make use of bitnami docker zookeeper and kafka images to setup the Kafka environment.
For bitnami docker reference: https://hub.docker.com/r/bitnami/kafka/

## Installation

Follow the below steps to Run the application.

Pull the project to the 'Project directory'

change directory to the working directory.

> mvn clean install

> docker-compose up -d

check if all the containers are up and running. Once all the containers are up and started.

> docker exec -it kafka /bin/sh   // this command will get the cli to kafka container

run the below commands on the kafka container

> cd /opt/bitnami/kafka/bin

> kafka-topics.sh --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 1 --topic test-topic

> kafka-console-consumer.sh -bootstrap-server localhost:9092 -topic test-topic -group test-group

kafka-console-consumer is a console cli, hence you'll have to manually terminate it with "CTRL + C". The above two commands will create the topic and group for the project.

Now you can send a message to the application via postman or any rest client.

CURL sample:

curl -i -X POST \
   -H "Content-Type:application/json" \
   -d \
'' \
 'http://localhost:8080/kafka/publish?message=My+first+message'
 
 to check the log within the application use the command
 
 > docker logs -f kafka-pub-sub-api

## Authors
Anoop Ramakrishnan - anoop00rkn@gmail.com , kanooprkn@gmail.com
